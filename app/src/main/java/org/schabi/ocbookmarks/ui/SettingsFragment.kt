package org.schabi.ocbookmarks.ui

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import org.schabi.ocbookmarks.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.app_preferences, rootKey)
    }
}